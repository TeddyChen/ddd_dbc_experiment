# DDD DBC Experiment
This is the experiment source in the paper "*Contracting the Aggregate Roots for Correctness in Domain-Driven Design*".

## Outline
- [Branches introduce](#branches)
- [Before you run](#before-you-run)
- [How to run](#how-to-run)
- [Project structure](#project-structure)
- [Note](#note)

## Branches introduce
There are five branches
1. [`master`](https://gitlab.com/TeddyChen/ddd_dbc_experiment) for the PIT report and README.md about the experiment.
1. [`Aggregate-UT`](https://gitlab.com/TeddyChen/ddd_dbc_experiment/-/tree/Aggreagte-UT) for the source of **Aggregate-UT**. 
1. [`Root-UT`](https://gitlab.com/TeddyChen/ddd_dbc_experiment/-/tree/Root-UT) for the source of **Root-UT**. 
1. [`Root-Contracting`](https://gitlab.com/TeddyChen/ddd_dbc_experiment/-/tree/Root-Contracting) for the source of **Root-Contracting**. 
1. [`benign_mutants`](https://gitlab.com/TeddyChen/ddd_dbc_experiment/-/tree/benign_mutants) for the benign mutants in the experiment.

## Before you run
1. Please install JDK 21. 
1. run ```mvn (clean) install``` under the root directory of the project(this command only need to be run once at the beginning)
2. Set the mutation target classes, target tests, and report directory in `pom.xml` of each bounded context (module `account`, `board`, or `team`).

For example, run the strategy **Aggregate-UT** of _Workflow_ aggregate in _Kanban Board_ bounded context
- for target classes
```xml
<targetClasses>
    <!--                         ****Workflow****                    -->       
    <param>ntut.csie.sslab.ezkanban.kanban.workflow.entity.Workflow</param>
    <param>ntut.csie.sslab.ezkanban.kanban.workflow.entity.factory.LaneBuilder</param>
    <param>ntut.csie.sslab.ezkanban.kanban.workflow.entity.CommittedCard</param>
    <param>ntut.csie.sslab.ezkanban.kanban.workflow.entity.Lane</param>
    <param>ntut.csie.sslab.ezkanban.kanban.workflow.entity.LaneId</param>
    <param>ntut.csie.sslab.ezkanban.kanban.workflow.entity.LaneType</param>
    <param>ntut.csie.sslab.ezkanban.kanban.workflow.entity.Stage</param>
    <param>ntut.csie.sslab.ezkanban.kanban.workflow.entity.SwimLane</param>
    <param>ntut.csie.sslab.ezkanban.kanban.workflow.entity.VoidLane</param>
    <param>ntut.csie.sslab.ezkanban.kanban.workflow.entity.WipLimit</param>
    <param>ntut.csie.sslab.ezkanban.kanban.workflow.entity.WorkflowEvents</param>
    <param>ntut.csie.sslab.ezkanban.kanban.workflow.entity.WorkflowId</param>
</targetClasses>
```

- for target tests
```xml
<targetTests>
    <!--                    ****Workflow****                    -->
    <param>ntut.csie.sslab.ezkanban.kanban.workflow.entity.*</param>
</targetTests>

```

- for report directory
```XML
<reportsDirectory>${project.build.directory}/pit-reports/Aggregate-UT/Workflow</reportsDirectory>
```

## How to run?
1. ```cd ${BOUNDED_CONTEXT}```, where ${BOUNDED_CONTEXT} is *account*, *board*, or *team*
2. ```mvn clean test-compile org.pitest:pitest-maven:mutationCoverage```
3. The default generated reports are located in `target/pit-reports/`

## Project structure
- [Bounded contexts](#bounded-contexts)
- [Verification assets](#verification-assets)
- [Source code](#source-code)

### Bounded contexts

The three bounded contexts *Account*, *Kanban Board*, and *Team* are located in the maven modules `account`, `team`, and `board`, respectively. 
The module `account` gets the aggregate *User*, the module `board` gets the four aggregates *Board*, *Card*, *Tag*, and *Workflow*, and the module `team` gets the aggregate *Team*.

### Verification assets

#### - Unit tests and Contract exercisers

- All the unit tests and contract exercisers are located in `src/test/java/.../${AGGREGATE}/entity/`.

#### - Contracts

- Contracts are co-located with the source code of the six aggregate root in the branch `Root-Contracting`. The six aggregate root files are `User.java`, `Board.java`, `Workflow.java`, `Card.java`, `Tag.java` or `Team.java`.

### Source code
- The domain objects of an aggregate are located under the path `src/main/.../${AGGREGATE}/entity/`. The class of the aggregate root is named as its aggregate name, for example, the class `Workflow` is the aggregate root of the aggregate _Workflow_, located in the `src/main/.../workflow/entity/Workflow.java`.

## Note
- This repository is only for the experiment so all the unrelated codes are removed.

- SLOC calculates the *Source Code Line* (excluding blank lines and comment lines) by the plugin [*statistic*](https://plugins.jetbrains.com/plugin/4509-statistic) of IntelliJ.
  - Unit tests calculate the files in `src/test/java/.../${AGGREGATE}/entity/`.
  - Contracts calculate the `require*`, `ensure*`, `old` and the private methods for contracts' assertion (those private methods are named with `_*` at the beginning).
  - Contract exercisers calculate the files in `src/test/java/.../${AGGREGATE}/entity/` in the branch `Root-Contracting`.




